package cl.ufro.dci.sra346serviciosautentificaciontest;


import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;

import java.io.IOException;
import java.util.Map;

public class RequestService {
    //ESTOS 2 METODOS NO TOCAR

    public static String getJWTProfesor() throws Exception {
        RequestBody formBody = new FormBody.Builder().
                add("username", "oancan@ufrontera.cl").
                add("password", "12345").
                build();
        String raw = sendPostReturnRawJson("http://localhost:8080/api/login",formBody);
        return jsonStringToMap(raw).get("token");
    }
    public static String getJWTEstudiante() throws Exception {
        RequestBody formBody = new FormBody.Builder().
                add("username", "a.melillan02@ufromail.cl").
                add("password", "12345").
                build();
        String raw = sendPostReturnRawJson("http://localhost:8080/api/login",formBody);
        return jsonStringToMap(raw).get("token");
    }
    public static String getJWTEstudiante2() throws Exception {
        RequestBody formBody = new FormBody.Builder().
                add("username", "j.echeverria02@ufromail.cl").
                add("password", "12345").
                build();
        String raw = sendPostReturnRawJson("http://localhost:8080/api/login",formBody);
        return jsonStringToMap(raw).get("token");
    }
    public static Response doPostWithJWT(String token, String url, RequestBody formBody) throws IOException {
        Request request = new Request.Builder().
                url(url).
                addHeader("User-Agent", "OkHttp Bot").
                addHeader("Authorization","Bearer " + token).
                post(formBody).
                build();

        try (Response response = httpClient.newCall(request).execute()) {
            return response;
        }
    }

    private static final OkHttpClient httpClient = new OkHttpClient();

    public static String sendPostReturnRawJson(String url, RequestBody formBody) throws Exception {
        Request request = new Request.Builder()
                .url(url)
                .addHeader("User-Agent", "OkHttp Bot")
                .post(formBody)
                .build();

        try (Response response = httpClient.newCall(request).execute()) {
            return response.body().string();
        }
    }


    private static Map<String,String> jsonStringToMap(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Map.class);
    }

    public static Response get(String params,String token) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://localhost:8080/asistencia/"+params)
                .get()
                .addHeader("Authorization", "Bearer "+token)
                .addHeader("Content-Type", "application/json")
                .build();
        return client.newCall(request).execute();
    }

    public static Response post(String params,RequestBody requestBody,String token) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://localhost:8080/asistencia/"+params)
                .post(requestBody)
                .addHeader("Authorization", "Bearer "+token)
                .addHeader("Content-Type", "application/json")
                .build();
        return client.newCall(request).execute();
    }
}
