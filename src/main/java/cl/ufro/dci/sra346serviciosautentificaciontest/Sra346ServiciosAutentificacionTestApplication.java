package cl.ufro.dci.sra346serviciosautentificaciontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sra346ServiciosAutentificacionTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sra346ServiciosAutentificacionTestApplication.class, args);
    }

}
