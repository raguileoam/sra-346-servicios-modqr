package cl.ufro.dci.sra346serviciosautentificaciontest;

import okhttp3.*;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.assertEquals;

public class AsistenciaServicioTest {
    //PARA REALIZAR LA PRUEBAS DEBE ESTAR EN EJECUCION
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    @Test
    public void testPublicarClaseNoProfesor() throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create("{\n\"asigId\":1,\n\"profesorId\":87654321L\n}",mediaType);
        Request request = new Request.Builder()
                .url("http://localhost:8080/asistencia/publicarClase/")
                .method("POST", body)
                .addHeader("Authorization", "Bearer "+RequestService.getJWTProfesor())
                .addHeader("Content-Type", "application/json")
                .build();
        Response response1=client.newCall(request).execute();
        response1.close();
        Response response = client.newCall(request).execute();
        assertEquals("Error 504: asignatura no pertenece al profesor ",response.body().string());
    }

    @Test
    public void testAsignaturanoencontrada() throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create("{\n\"asigId\":3,\n\"profesorId\":87654321L\n}",mediaType);
        Request request = new Request.Builder()
                .url("http://localhost:8080/asistencia/publicarClase/")
                .method("POST", body)
                .addHeader("Authorization", "Bearer "+RequestService.getJWTProfesor())
                .addHeader("Content-Type", "application/json")
                .build();
        Response response1=client.newCall(request).execute();
        response1.close();
        Response response = client.newCall(request).execute();
        assertEquals("Error 501: Asignatura no encontrada",response.body().string());
    }

    @Test
    public void testPublicarClaseDosVeces() throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create("{\n\"asigId\":2,\n\"profesorId\":87654321L\n}",mediaType);
        Request request = new Request.Builder()
                .url("http://localhost:8080/asistencia/publicarClase/")
                .method("POST", body)
                .addHeader("Authorization", "Bearer "+RequestService.getJWTProfesor())
                .addHeader("Content-Type", "application/json")
                .build();
        Response response1=client.newCall(request).execute(); //Primera publicacion
        response1.close();
        Response response = client.newCall(request).execute(); //Segunda publicacion
        assertEquals("Error 502: Asignatura tiene un codigo QR activo",response.body().string());
    }
    @Test
    public void testVerQrNoSeEncuentraID() throws Exception {
        Response response = RequestService.get("verQR/7",RequestService.getJWTEstudiante());
        assertEquals("Error 601: No se encuentra codigo qr para el idClase ingresado ",response.body().string());
    }

    @Test
    public void testVRegistrarClaseNoEncontrada() throws Exception {
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n\t\"idClase\":100,\n\t\"token\":\"lw7hXBQdBñucjhL\",\n\t\"numeroMatricula\":\"20102742k17\"\n}");
        Response response=RequestService.post("registrarCodigoQR/",body,RequestService.getJWTEstudiante());
        assertEquals("Error 404: idClase no encontrada",response.body().string());
    }

    @Test
    public void testVerQrOk() throws Exception {
        Response response=RequestService.get("verQR/4",RequestService.getJWTEstudiante());
        assertEquals(HttpStatus.OK.value(),response.code());
    }
    @Test
    public void testRutOk() throws Exception {
        RequestBody formBody = new FormBody.Builder().
                add("email", "a.melillan02@ufromail.cl").
                build();
        assertEquals(HttpStatus.OK.value(), RequestService.doPostWithJWT(RequestService.getJWTProfesor(),"http://localhost:8080/api/rut",formBody).code());
    }

}
